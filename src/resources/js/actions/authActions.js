import {
  USER_LOADING,
  USER_LOADED,
  USER_LOADING_ERROR,
  REGISTER_SUCCESS,
  LOGIN_SUCCESS,
  USER_LOGOUT,
  SET_LOADING,
  FINISH_LOADING
} from './types';

import axios from 'axios';

export const loadUser = () => (dispatch, getState) => {
  return new Promise(async (resolve, reject) => {
    dispatch({ type: USER_LOADING });
    try {
      const res = await axios.get('/api/auth/user', tokenConfig(getState));
      dispatch({ type: USER_LOADED, payload: { user: res.data.user } });
      resolve();
    } catch (err) {
      dispatch({ type: USER_LOADING_ERROR });
      reject();
    }
  });
};

export const register = payload => (dispatch, getState) => {
  return new Promise(async (resolve, reject) => {
    try {
      const res = await axios.post('/api/auth/register', payload);
      resolve();
      dispatch({
        type: REGISTER_SUCCESS,
        payload: res.data
      });
    } catch (err) {
      reject(err.response.data.error_msg);
    }
  });
};

export const login = payload => (dispatch, getState) => {
  return new Promise(async (resolve, reject) => {
    try {
      const res = await axios.post('/api/auth/login', payload);
      resolve();
      dispatch({ type: LOGIN_SUCCESS, payload: res.data });
    } catch (err) {
      reject(err.response.data.error_msg);
    }
  });
};

export const logout = () => (dispatch, getState) => {
  return new Promise((resolve, reject) => {
    dispatch({ type: USER_LOGOUT });
    resolve();
  });
};

export const tokenConfig = getState => {
  const token = getState().auth.token;

  const config = {
    headers: {
      'Content-type': 'application/json'
    }
  };

  if (token) {
    config.headers['x-auth-token'] = token;
  }

  return config;
};
