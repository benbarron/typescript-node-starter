import { POSTS_LOADED, POSTS_LOADING } from './types';
import { showLoading, hideLoading } from 'react-redux-loading-bar';
import axios from 'axios';
import { tokenConfig } from './authActions';

export const loadPosts = () => (dispatch, getState) => {
  return new Promise(async (resolve, reject) => {
    dispatch(showLoading());

    dispatch({
      type: POSTS_LOADING
    });

    try {
      const result = await axios.get('/api/posts', tokenConfig(getState));

      setTimeout(() => {
        dispatch({
          type: POSTS_LOADED,
          payload: result.data
        });

        dispatch(hideLoading());
        resolve();
      }, 2000);
    } catch (err) {
      console.log(err);
      reject(err.response.data.error_msg);
    }
  });
};
