import React, { Fragment, useEffect } from 'react';
import { Switch, Route } from 'react-router-dom';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { loadUser } from './actions/authActions';
import { default as routes } from './routes';

import NavBar from './components/NavBar';

import { default as LoadingBar } from './components/LoadingBar';

const App = props => {
  useEffect(() => {
    props
      .loadUser()
      .then(() => {})
      .catch(() => {});
  }, []);

  let rts = routes;

  if (!props.auth.isAuthenticated) {
    rts = rts.filter(route => !route.auth);
  }

  return (
    <Fragment>
      <Fragment>
        <NavBar />
        <LoadingBar />
        <Switch>
          {rts.map((route, i) => (
            <Route
              key={i}
              path={route.path}
              exact={route.exact}
              component={route.component}
            />
          ))}
        </Switch>
      </Fragment>
    </Fragment>
  );
};

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};

export default withRouter(connect(mapStateToProps, { loadUser })(App));
