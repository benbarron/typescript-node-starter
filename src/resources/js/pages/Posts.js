import React, { Fragment, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import { Card, CardHeader } from '@material-ui/core';

import { loadPosts } from './../actions/postActions';

const Posts = props => {
  useEffect(() => {
    props
      .loadPosts()
      .then(() => {})
      .catch(err => {
        console.log(err);
      });
  }, []);

  return (
    <Fragment>
      <div className="container mt-5">
        {!props.posts.isLoading ? (
          <Fragment>
            {props.posts.posts.map((post, i) => (
              <Card key={i} className="mb-4">
                <CardHeader title={post.title} />
              </Card>
            ))}
          </Fragment>
        ) : null}
      </div>
    </Fragment>
  );
};

const mapStateToProps = state => {
  return {
    posts: state.posts
  };
};

export default withRouter(connect(mapStateToProps, { loadPosts })(Posts));
