import React, { Fragment } from 'react';
import { withRouter } from 'react-router-dom';
import { Card } from '@material-ui/core';

export default withRouter(props => {
  return (
    <Fragment>
      <div className="container mt-5">
        <Card>
          <div className="p-4">
            <h1>Page Not Found</h1>
          </div>
        </Card>
      </div>
    </Fragment>
  );
});
