import React from 'react';
import { connect } from 'react-redux';

import { Card } from '@material-ui/core';

const Home = props => {
  return (
    <div className="container mt-5">
      <Card>
        <div className="p-4">
          <h1>
            {props.auth.isAuthenticated
              ? 'Logged in as ' + props.auth.user.name
              : 'Not logged in'}
          </h1>
        </div>
      </Card>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};

export default connect(mapStateToProps, {})(Home);
