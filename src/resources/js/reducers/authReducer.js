import {
  USER_LOADED,
  USER_LOADING,
  USER_LOADING_ERROR,
  REGISTER_SUCCESS,
  LOGIN_SUCCESS,
  USER_LOGOUT,
  SET_LOADING,
  FINISH_LOADING
} from './../actions/types';

const initialState = {
  token: localStorage.getItem('auth-token'),
  isAuthenticated: false,
  isLoading: false,
  user: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case USER_LOADING:
      return {
        ...state,
        isLoading: true
      };
    case USER_LOADED:
      return {
        ...state,
        isLoading: false,
        isAuthenticated: true,
        user: action.payload.user
      };

    case USER_LOADING_ERROR:
      localStorage.removeItem('auth-token');

      return {
        ...state,
        isLoading: false,
        isAuthenticated: false,
        user: null
      };

    case REGISTER_SUCCESS:
    case LOGIN_SUCCESS:
      localStorage.setItem('auth-token', action.payload.token);

      return {
        ...state,
        isAuthenticated: true,
        user: action.payload.user,
        token: action.payload.token
      };

    case USER_LOGOUT:
      localStorage.removeItem('auth-token');

      return {
        ...state,
        isAuthenticated: false,
        user: null,
        token: null
      };

    case SET_LOADING:
      return {
        ...state,
        isLoading: true
      };

    case FINISH_LOADING:
      return {
        ...state,
        isLoading: false
      };
    default:
      return state;
  }
};
