import { combineReducers } from 'redux';
import authReducer from './authReducer';
import postReducer from './postReducer';
import { loadingBarReducer } from 'react-redux-loading-bar';

export default combineReducers({
  auth: authReducer,
  posts: postReducer,
  loadingBar: loadingBarReducer
});
