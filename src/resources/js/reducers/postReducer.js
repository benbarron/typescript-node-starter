import { POSTS_LOADED, POSTS_LOADING } from './../actions/types';

const initialState = {
  posts: [],
  isLoading: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case POSTS_LOADING:
      return {
        ...state,
        isLoading: true,
        posts: []
      };
    case POSTS_LOADED:
      return {
        ...state,
        isLoading: false,
        posts: action.payload.posts
      };
    default:
      return {
        ...state
      };
  }
};
