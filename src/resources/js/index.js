import React from 'react';
import ReactDOM from 'react-dom';

import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';

import App from './app';
import { default as store } from './store';

const Index = () => {
  return (
    <Provider store={store}>
      <Router>
        <App />
      </Router>
    </Provider>
  );
};

if (document.getElementById('root')) {
  ReactDOM.render(<Index />, document.getElementById('root'));
}
