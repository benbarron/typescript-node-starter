import Home from './pages/Home';
import Posts from './pages/Posts';
import Users from './pages/Users';
import NotFoundPage from './pages/NotFoundPage';

export default [
  {
    name: 'Home',
    path: '/',
    exact: true,
    component: Home,
    icon: 'fas fa-home',
    auth: false
  },
  {
    name: 'Posts',
    path: '/posts',
    exact: true,
    component: Posts,
    icon: 'fas fa-book-reader',
    auth: true
  },
  {
    name: 'Users',
    path: '/users',
    exact: true,
    component: Users,
    icon: 'fas fa-users',
    auth: true
  },
  {
    name: '404',
    path: '*',
    exact: false,
    component: NotFoundPage,
    icon: null,
    auth: false
  }
];
