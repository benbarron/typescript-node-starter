import React, { Fragment, useState } from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';

import { register } from './../actions/authActions';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

const RegisterModal = props => {
  const [open, setOpen] = useState(false);
  const [msg, setMsg] = useState('');
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const register = () => {
    if (!name || !email || !password) {
      setMsg('Please enter all fields');
      return setTimeout(() => setMsg(''), 4000);
    }

    props
      .register({ name, email, password })
      .then(res => {})
      .catch(err => {
        setMsg(err);

        setTimeout(() => setMsg(''), 4000);
      });
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Fragment>
      <Button variant="outlined" color="inherit" onClick={handleClickOpen}>
        Register
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
        maxWidth={'sm'}
        fullWidth={true}
      >
        <DialogTitle id="form-dialog-title">Register</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="normal"
            id="name"
            label="Name"
            type="text"
            value={name}
            onChange={e => setName(e.target.value)}
            fullWidth
          />

          <TextField
            autoFocus
            margin="normal"
            id="email"
            label="Email"
            type="email"
            value={email}
            onChange={e => setEmail(e.target.value)}
            fullWidth
          />

          <TextField
            autoFocus
            margin="normal"
            id="password"
            label="Password"
            type="password"
            value={password}
            onChange={e => setPassword(e.target.value)}
            fullWidth
          />

          <span style={{ color: '#aa0000', height: 50 }}>{msg ? msg : ''}</span>

          <hr />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="default" variant="outlined">
            Cancel
          </Button>
          <Button onClick={register} color="primary" variant="outlined">
            Register
          </Button>
        </DialogActions>
      </Dialog>
    </Fragment>
  );
};

const mapStateToProps = state => {
  return {};
};

export default withRouter(
  connect(mapStateToProps, { register })(RegisterModal)
);
