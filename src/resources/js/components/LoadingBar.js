import React from 'react';
import LoadingBar from 'react-redux-loading-bar';

export default () => {
  return (
    <header>
      <LoadingBar />
    </header>
  );
};
