import React, { Fragment } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
/* import actions */
import { logout } from './../actions/authActions';
/* import material-ui */
import { makeStyles } from '@material-ui/core/styles';
import { AppBar, Toolbar, Typography, Button } from '@material-ui/core';
/* import child components */
import { default as RegisterModal } from './RegisterModal';
import { default as LoginModal } from './LoginModal';
import { default as SideDrawerMenu } from './SideDrawerMenu';
import { default as routes } from './../routes';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  title: {
    flexGrow: 1
  }
}));

const NavBar = props => {
  const classes = useStyles();
  const { auth } = props;

  const logout = () => {
    props
      .logout()
      .then(() => {
        let redirectPath;

        // find first route path that does not require authentication
        // and redirect the use to that route
        for (let i = 0; i < routes.length; i++) {
          if (!routes[i].auth) {
            redirectPath = routes[i].path;
            break;
          }
        }

        props.history.push(redirectPath);
      })
      .catch(err => {});
  };

  return (
    <div className={classes.root}>
      <AppBar
        position="static" //color="primary"
        style={{ background: '#2f3640' }}
      >
        <Toolbar>
          <SideDrawerMenu />
          <Typography variant="h6" className={classes.title}></Typography>
          <div>
            {auth.isAuthenticated ? (
              <Fragment>
                <Button color="inherit" variant="text" className="mr-3">
                  {auth.user.name}
                </Button>
                <Button color="inherit" variant="outlined" onClick={logout}>
                  Logout
                </Button>
              </Fragment>
            ) : (
              <Fragment>
                <LoginModal />
                <RegisterModal />
              </Fragment>
            )}
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};

export default withRouter(connect(mapStateToProps, { logout })(NavBar));
