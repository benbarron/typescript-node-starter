import React, { Fragment, useState } from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';

import { login } from './../actions/authActions';

import Button from '@material-ui/core/Button';
import {
  TextField,
  Dialog,
  DialogActions,
  DialogTitle,
  DialogContent
} from '@material-ui/core/';

const LoginModal = props => {
  const [open, setOpen] = useState(false);
  const [msg, setMsg] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const login = () => {
    if (!email || !password) {
      setMsg('Please enter all fields');
      return setTimeout(() => setMsg(''), 4000);
    }

    props
      .login({ email, password })
      .then(() => {
        // try {
        //   setOpen(false);
        // } catch (e) {}
      })
      .catch(err => {
        setMsg(err);
      });
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Fragment>
      <Button
        variant="outlined"
        color="inherit"
        onClick={handleClickOpen}
        style={{ marginRight: 10 }}
      >
        Login
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        maxWidth={'sm'}
        fullWidth={true}
      >
        <DialogTitle id="customized-dialog-title">Login</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="normal"
            id="email"
            label="Email"
            type="email"
            value={email}
            onChange={e => setEmail(e.target.value)}
            fullWidth
          />

          <TextField
            autoFocus
            margin="normal"
            id="password"
            label="Password"
            type="password"
            value={password}
            onChange={e => setPassword(e.target.value)}
            fullWidth
          />

          <span style={{ color: '#aa0000', height: 50 }}>{msg ? msg : ''}</span>

          <hr />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="default" variant="outlined">
            Cancel
          </Button>
          <Button onClick={login} color="primary" variant="outlined">
            Login
          </Button>
        </DialogActions>
      </Dialog>
    </Fragment>
  );
};

const mapStateToProps = state => {
  return {};
};

export default withRouter(connect(mapStateToProps, { login })(LoginModal));
