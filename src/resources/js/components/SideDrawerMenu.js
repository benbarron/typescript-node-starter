import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import { IconButton } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';

import { Link, withRouter } from 'react-router-dom';
import { default as routes } from './../routes';
import { connect } from 'react-redux';

const useStyles = makeStyles({
  list: {
    width: 250
  },
  fullList: {
    width: 'auto'
  }
});

const SideDrawerMenu = props => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);

  const toggleDrawer = e => {
    setOpen(!open);
  };

  const followLink = path => {
    setOpen(false);
    props.history.push(path);
  };

  let rts = routes;

  if (!props.auth.isAuthenticated) {
    rts = rts.filter(route => !route.auth);
  }

  const sideList = side => (
    <div className={classes.list} role="presentation" onClick={toggleDrawer}>
      {/* <Divider /> */}
      <List>
        {rts.map((route, i) =>
          route.icon ? (
            <ListItem button key={i} onClick={e => followLink(route.path)}>
              <ListItemIcon>
                <i className={route.icon}></i>
              </ListItemIcon>
              <ListItemText primary={route.name} />
            </ListItem>
          ) : null
        )}
      </List>
    </div>
  );

  return (
    <div>
      <IconButton
        edge="start"
        className={classes.menuButton}
        color="inherit"
        aria-label="menu"
        onClick={toggleDrawer}
      >
        <MenuIcon />
      </IconButton>
      <Drawer anchor="left" open={open} onClose={toggleDrawer}>
        {sideList('left')}
      </Drawer>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};

export default withRouter(connect(mapStateToProps, {})(SideDrawerMenu));
