import { Request, Response, NextFunction } from 'express';
import * as jwt from 'jsonwebtoken';
import { UserModel, User } from './../models/User';

export default async (req: Request, res: Response, next: NextFunction) => {
  const token: string | undefined = req.header('x-auth-token');

  if (!token) {
    return res.status(401).json({ msg: 'No token, authorizaton denied' });
  }

  try {
    const decoded: any = jwt.verify(token, String(process.env.JWT_SECRET));  
    
    const user: UserModel | null = await User.findOne({ _id: String(decoded.id) }).select('-password');

    if(!user) {
      return res.status(400).json({ msg: 'Token is not valid' });
    }

    req.user = user;
  } catch (e) {
    return res.status(400).json({ msg: 'Token is not valid' });
  }

  next();
}

