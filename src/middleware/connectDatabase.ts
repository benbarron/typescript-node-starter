import mongoose from 'mongoose';

export default async () => {
  try {
    const uri: string = String(process.env.MONGO_URI);

    const options: Object = {
      useNewUrlParser: true,
      useCreateIndex: true,
      useUnifiedTopology: true
    };

    await mongoose.connect(uri, options);
  } catch (e) {
    console.error(e);
    process.exit(1);
  }
};