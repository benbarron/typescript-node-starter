import { Schema, Document, model } from 'mongoose';
import * as bcrypt from 'bcryptjs';
import { NextFunction } from 'express';

interface User {
  name: string;
  email: string;
  password: string;
  dateCreated: Date;
}

export interface UserModel extends User, Document {}

declare global {
  namespace Express {
    interface User extends UserModel {}
  }
}

export const UserSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  dateCreated: {
    type: Date,
    default: Date.now()
  }
});

UserSchema.pre('save', async function(next) {
  const user = this as UserModel;

  if (!user.isModified('password')) {
    return next();
  }

  const salt: string = await bcrypt.genSalt(10);
  const hash: string = await bcrypt.hash(user.password, salt);

  user.password = hash;

  next();
});

UserSchema.pre(/deleteOne|deleteMany|findOneAndDelete/, async function(next: NextFunction) {
  const user = this as any;

  next();
});

export const User = model<UserModel>('users', UserSchema);
