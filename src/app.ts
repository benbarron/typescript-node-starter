import express, { Application } from 'express';
import path from 'path';

import { default as connectDatabase } from './middleware/connectDatabase';
import { default as authRoutes } from './controllers/auth';
import { default as postRoutes } from './controllers/posts';
import { default as rootRoutes } from './controllers/root';
import { default as checkAuth } from './middleware/checkAuth'

const app: Application = express();

connectDatabase();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use('/assets', express.static(path.resolve(__dirname, 'public')))
app.use('/api/auth', authRoutes);
app.use('/api/posts', checkAuth, postRoutes);
app.use(rootRoutes);

export default app;
