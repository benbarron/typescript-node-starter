/*
 *
 *
 *
 */

/* declaring the require function allows us to use it to import 
  non typescript modules for use without getting warnings. */
declare function require(name: string): any;
import cluster from 'cluster';
import os, { CpuInfo } from 'os';
import http from 'http';
import https from 'https';
import dotenv from 'dotenv';
import { UserModel } from './models/User'

dotenv.config();

declare global {
  namespace Express {
    export interface Request {
      user: UserModel;
    }
  }
  namespace process {
    export interface env extends Object {}
  }
}

import { default as app } from './app';

if (process.env.MODE == 'PRODUCTION') {
  if (cluster.isMaster) {
    const cpus: Array<CpuInfo> = os.cpus();

    for (let i: number = 0; i < cpus.length; i++) {
      cluster.fork();
    }
  } else {
    const options: Object = {
      key: '',
      cert: ''
    };

    http.createServer(app).listen(Number(process.env.HTTP_PORT));
    https.createServer(options, app).listen(Number(process.env.HTTPS_PORT));
  }
} else {
  const port: number = Number(process.env.DEV_PORT);
  const mode: string = String(process.env.MODE).toLowerCase();
  app.listen(port, () => console.log('Application running in ' + mode + ' mode on port %d.\nClick to open http://localhost:%d.', port, port));
}

