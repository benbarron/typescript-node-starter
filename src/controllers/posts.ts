import { Router, Request, Response } from 'express';
import fs from 'fs';
import path from 'path';

const route: Router = Router();

route.get('/', async(req: Request, res: Response) => {
    fs.readFile(path.resolve(__dirname, '../data/posts.json'), (err, data) => {
      if(err) {
        return res.status(400).json({ error_msg: 'Error fetching posts.' });
      }

      return res.json({ posts: JSON.parse(data.toString()) })
    })
});

export default route;