import { Router, Request, Response } from 'express';
import * as bcrypt from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
import { default as checkAuth } from './../middleware/checkAuth'
import { UserModel, User } from './../models/User';

const route: Router = Router();

route.get('/user', checkAuth, async (req: Request, res: Response) => {
  return res.json({ user: req.user })
});

route.post('/register', async (req: Request, res: Response) => {
  const name: string = req.body.name;
  const email: string = req.body.email;
  const password: string = req.body.password;

  if(!name || !email || !password) {
    return res.status(400).json({ error_msg: 'Please enter all fields'})
  }

  const existing: UserModel | null = await User.findOne({ email });

  if(existing) {
    return res.status(400).json({ error_msg: 'Email address is already in use.' })
  }

  const user: UserModel = new User({ name, email, password });

  try {
    await user.save();
  } catch(e) {
    return res.status(400).json({ error_msg: 'Error saving the user'})
  }

  const token = await jwt.sign({ id: user._id }, String(process.env.JWT_SECRET), { expiresIn: Number(process.env.JWT_LIFETIME) });

  return res.json({ 
    success_msg: 'User registration sucessful', 
    token, 
    user: {
      name: user.name,
      email: user.email
    }
  })
});

route.post('/login', async (req: Request, res: Response) => {
  const email: string = req.body.email;
  const password: string = req.body.password;

  if(!email || !password) {
    return res.status(400).json({ error_msg: 'Please enter all fields.' })
  }

  const user: UserModel | null = await User.findOne({ email });

  if(!user) {
    return res.status(400).json({ error_msg: 'Invalid account details' });
  }

  const match: Boolean = await bcrypt.compare(password, user.password);

  if(!match) {
    return res.status(400).json({ error_msg: 'Invalid account details' });
  }

  const token = await jwt.sign({ id: user._id }, String(process.env.JWT_SECRET), { expiresIn: Number(process.env.JWT_LIFETIME) });

  return res.json({ 
    success_msg: 'OK', 
    token, 
    user: {
      name: user.name,
      email: user.email
    }
  })
});

export default route;