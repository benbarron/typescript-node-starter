import {Router, Request, Response} from 'express';
import path from 'path';

const route: Router = Router();

const allowedRoutes: Array<string> = [
  '/', 
  '/:all',
  '/:all/:routes',
  '/:all/:the/:routes'
];

route.get(allowedRoutes, async(req: Request, res: Response) => {
  return res.sendFile(path.resolve(__dirname, '../views/index.html'));
});

export default route;