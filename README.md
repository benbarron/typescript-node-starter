# Typescript and NodeJS Starter

---

## Download and Install

```
$ git clone https://gitlab.com/benbarron/typescript-node-starter.git
$ cd typescript-node-starter
$ npm i
$ nano .env ## change env variables
$ npm run server:dev
```

---

## NPM Scripts

- start - runs the production server run this after compiling typescript
- server - runs the server in dev mode using ts-node
- server:dev - watches the server using nodemon
- server:build - compiles typescript and puts it in dist folder
- assets:build - compiles the front end assets using Laravel mix, for development
- assets:watch - runs assets:build with a --watch flag
  -- assets:prod - compiles front end assets for production
  -- bundle - builds front end assets then compiles typescript into dist folder

---

## Tech Stack Used

- NodeJS
- Typescript
- Express
- React & Redux
- MongoDB and Mongoose ORM
- Json Web Tokens

---
