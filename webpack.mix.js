const mix = require('laravel-mix');

mix
  .sass('./src/resources/sass/main.scss', './src/public/css/app.css')
  .react('./src/resources/js', './src/public/js/app.js');
